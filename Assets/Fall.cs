using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*public class Particule
{
    [InspectorName("force")]
    public Vector3 force;
    [InspectorName("particule")]
    public  GameObject particul;

}*/



public class Fall : MonoBehaviour
{
    private const float g = 9.81f;
    public float mass = 3f;
    public float k = 7f;
    Vector2 gameobjects;
    public float damping=10f;
    public float x_Space, y_Space, x_Start, y_Start;
    public int column =9, row = 9;
    public GameObject Prefab;
    public float Length=2;
    float timeElapse;
    struct Particule
    {
        public Vector3 force;
        public Vector3 acc;
        public GameObject particul;
        public Vector3 Velocity;
    }

     Particule[,] particules;


    public void Start()
    {
        gameobjects = new Vector2(column, row);
      
        
        particules = new Particule[(int)gameobjects.x, (int)gameobjects.y];
        for (int i = 0; i < gameobjects.x; i++)
        {
            for (int j = 0; j < gameobjects.y; j++)
            {
                float pos_x = x_Space * (x_Start + i);
                float pos_z = y_Space * (y_Start - j);
                GameObject sphere = Instantiate(Prefab, new Vector3(pos_x, 0, pos_z), Quaternion.identity);
                particules[i,j].particul = sphere;            
                particules[i, j].force = Vector3.zero;
                particules[i, j].acc = Vector3.zero;

            }
        }

    }


    public void FixedUpdate()
    {
        timeElapse += Time.fixedDeltaTime;
        LinkingSprings();
        ForceApplication();
    }

    public void ForceApplication()
    {
        for (int i = 0; i < gameobjects.x; i++)
            for (int j = 0; j < gameobjects.y; j++)
            {
                Force(particules[i, j].force, i, j);
               
            }
    }
    public void LinkingSprings()
    {
        for (int i = 0; i < gameobjects.x; i++)
            for (int j = 0; j < gameobjects.y; j++)
            {
                
                
                StructuralSprings(i, j);
                ShearSpring(i, j);
                flexionSprings(i, j);
            }
    }

    public void StructuralSprings(int i ,int j)
    {
       
        if (i < gameobjects.x - 1)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i + 1, j].particul,1);
            Debug.DrawLine(particules[i,j].particul.transform.position, particules[i+1, j].particul.transform.position, Color.red);
        }
        if (i > 0)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i - 1, j].particul,1);
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i - 1, j].particul.transform.position, Color.red);
        }
        if (j < gameobjects.y - 1)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i, j + 1].particul,1);
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i, j + 1].particul.transform.position, Color.red);
        }
        if (j > 0)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i, j - 1].particul,1);
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i , j - 1].particul.transform.position, Color.red);
        }
    }

    public void ShearSpring(int i,int j)
    {
      
        if (i < gameobjects.x - 1 && j < gameobjects.y - 1)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i + 1, j + 1].particul,Mathf.Sqrt(2));
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i + 1, j+1].particul.transform.position, Color.green);
        }
        if (i < gameobjects.x - 1 && j > 0)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i + 1, j - 1].particul,Mathf.Sqrt(2));
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i + 1, j - 1].particul.transform.position, Color.green);
        }
        if (i > 0 && j > 0)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i - 1, j - 1].particul,Mathf.Sqrt(2));
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i - 1, j - 1].particul.transform.position, Color.green);
        }
        if (i > 0 && j < gameobjects.y - 1)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i - 1, j + 1].particul,Mathf.Sqrt(2));
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i - 1, j + 1].particul.transform.position, Color.green);
        }
    }

    public void flexionSprings(int i,int j)
    {
      
        if (i < gameobjects.x - 2)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i + 2, j].particul,2);
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i + 2, j ].particul.transform.position, Color.blue);
        }
        if (i > 1)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i - 2, j].particul,2);
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i - 2, j].particul.transform.position, Color.blue);
        }
        if (j < gameobjects.y - 2)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i, j + 2].particul,2);
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i , j+2].particul.transform.position, Color.blue);
        }
        if (j > 1)
        {
            particules[i, j].force += GetForce(particules[i, j].particul, particules[i, j - 2].particul,2);
           
            Debug.DrawLine(particules[i, j].particul.transform.position, particules[i , j-2].particul.transform.position, Color.blue);
        }
       
    }
    Vector3 GetForce(GameObject ob1,GameObject ob2, float Coeff)
    {
        Vector3 L = ob2.transform.position - ob1.transform.position;
        Vector3 InternalForce = k * (L  - (Length * Coeff * (L / L.magnitude)));
        return InternalForce ;
    }

   void Force(Vector3 force,int i, int j)
    {
        float deltaT = Time.fixedDeltaTime;
      
        if(i == 0)
        {
            particules[i, j].acc = Vector3.zero;
        }
        else
        {

            particules[i, j].acc = (-damping * particules[i, j].acc * deltaT + force / timeElapse + new Vector3(0, -g * mass, 0)) / mass;

        }
        particules[i, j].Velocity = particules[i, j].acc * deltaT * 100;
        Debug.LogWarning(particules[i, j].acc);
        particules[i, j].particul.transform.position += new Vector3(transform.position.x + particules[i,
        j].Velocity.x * deltaT, transform.position.y + particules[i, j].Velocity.y * deltaT, transform.position.z);
    
    }
}
